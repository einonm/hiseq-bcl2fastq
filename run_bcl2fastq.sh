#! /bin/bash
#
# Run bcl2fastq efficiently on the Hawk cluster
#
# Copyright (C) 2019  Mark Einon <mark.einon@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# Version 2.1 only as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

SCRATCH=/scratch/`whoami`/bcl2fastq_source/
QSUB="sbatch --parsable --account=scw1422 --wait"
EMAIL=""
Q="compute"

usage()
{
cat << EOF
Usage: `basename $0` [OPTION]... <-R RUNFOLDER_DIR> <-s SAMPLESHEET>
Submits instances of bcl2fastq to the Cardiff University Hawk cluster to
generate fastq data from bcl files.

Mandatory arguments to long options are mandatory for short options too.
  -e, --email=EMAIL         email address to send completion messages
  -f, --final-dir=DIR       path to final output directory
  -p, --extra-opts=STR      quoted string of extra options to pass to bcl2fastq
  -R, --runfolder-dir=DIR   path to runfolder directory
  -s, --sample-sheet=PATH   path to a sample sheet file

  -h, --help                display this help and exit

Example:
    ./run_bcl2fastq.sh -R /gluster/neurocluster/sequencers/hiseq4000bk/181005_K00267_0181_AHW2KHBBXX \\
                       -e user@cf.ac.uk \\
                       -f /gluster/neurocluster/sequencers/client/ \\
                       -s /gluster/neurocluster/sequencers/client/SampleSheet.csv \\
                       -p "--minimum-trimmed-read-length 0 --mask-short-adapter-reads 0 --tiles s_[6-8]"

EOF
}

PARSEOPTS=`getopt -o he:R:f:p:s: --long help,email:,runfolder-dir:,final-dir:,extra-opts:,sample-sheet: \
           -n $0 -- "$@"`

while true; do
    case $1 in
        -h | --help )
            usage;
            exit 1
            ;;
        -e | --email )
            EMAIL=" --mail-user=$2 --mail-type=END"
            EMAIL_ADDR=$2
            echo Sending completion email to $2
            shift 2
            ;;
        -R | --runfolder-dir )
            RUN_DIR=$2
            BCL_DIR=`basename ${RUN_DIR}`
            shift 2
            ;;
        -f | --final-dir )
            # If not specified, will not copy results
            OUT_DIR=$2
            shift 2
            ;;
        -p | --extra-opts )
            EXTRA_STR=$2
            shift 2
            ;;
        -s | --sample-sheet )
            SAMPLESHEET=$2
            shift 2
            ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

# Create the input/output scratch area for bcl2fastq
if [ ! -d ${SCRATCH} ]; then
	mkdir ${SCRATCH}
fi

# Copy the raw bcl data to scratch from long-term storage
if [ -v BCL_DIR ]; then
	echo "Syncing input directory to scratch..."
    echo "${RUN_DIR}  to ${SCRATCH}"
    rsync -Ava --progress ${RUN_DIR} ${SCRATCH}
else
	echo "No input directory specified"
	usage
fi

echo "Copying samplesheet to scratch..."
rsync -Ava --progress ${SAMPLESHEET} ${SCRATCH}${BCL_DIR}/`basename ${SAMPLESHEET}`

# run bcl2fastq
# use --exclusive??
echo "Running bcl2fastq..."

JOB_RET=$($QSUB -o "${SCRATCH}${BCL_DIR}_fastq.slurm-%j.out" \
        -N1-1 \
        -n40 \
        -p ${Q} \
        ${EMAIL} \
        bcl2fastq.sh -R ${SCRATCH}${BCL_DIR} \
        -o ${SCRATCH}${BCL_DIR}_fastq \
        -s `basename ${SAMPLESHEET}` \
        -p "${EXTRA_STR}")

TMP_DIR=${SCRATCH}${BCL_DIR}_fastq.${JOB_RET}

# Move the slurm output log and samplesheet into long-term storage
mv ${SCRATCH}${BCL_DIR}_fastq.slurm-${JOB_RET}.out ${TMP_DIR}
mv ${SCRATCH}${BCL_DIR}/`basename ${SAMPLESHEET}` ${TMP_DIR}


if [ -v OUT_DIR ]; then
	echo "Syncing output directory from scratch..."
    rsync -Ava --progress ${TMP_DIR} ${OUT_DIR}
fi

echo "Hawk bcl2fastq job ${JOB_RET} finished

      Output: ${OUT_DIR}
      Sample sheet: ${SAMPLESHEET}
      Run dir: ${RUN_DIR}" | mail -s "Hawk bcl2fastq ${JOB_RET} finished" ${EMAIL_ADDR}
