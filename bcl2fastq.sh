#! /bin/bash
#
# Run bcl2fastq efficiently on the Hawk cluster
#
# Copyright (C) 2019  Mark Einon <mark.einon@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# Version 2.1 only as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

EXTRA_STR=""

while getopts "R:o:p:s:" OPTION
  do
    case $OPTION in
    R)
        BCL_DIR=$OPTARG
    ;;
    o)
	    OUT_DIR=$OPTARG
    ;;
    p)
	    EXTRA_STR="${OPTARG}"
    ;;
    s)
	    SAMPLESHEET=$OPTARG
    ;;
    ?)
        usage
        exit
    ;;
    esac
done

module load bcl2fastq/2.18

THREADS=80

bcl2fastq -R ${BCL_DIR} \
	  -o ${OUT_DIR}.${SLURM_JOB_ID} \
	  --sample-sheet ${BCL_DIR}/${SAMPLESHEET} \
	  --min-log-level TRACE \
      ${EXTRA_STR[@]}\
	  -r ${THREADS} \
	  -d ${THREADS} \
	  -p ${THREADS} \
      -w ${THREADS}

