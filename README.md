Runs instances of bcl2fastq on the Hawk cluster to generate fastq data from bcl files.                                                                        

Use the run_bcl2fastq.sh program to start - the bcl2fastq.sh program is submitted 
as a job by this script.
                                                                                                                                                              
Mandatory arguments to long options are mandatory for short options too.                                                                                      
  -e, --email=EMAIL         email address to send completion messages                                                                                         
  -f, --final-dir=DIR       path to final output directory                                                                                                    
  -R, --runfolder-dir=DIR   path to runfolder directory                                                                                                       
                                                                                                                                                              
  -h, --help                display this help and exit           